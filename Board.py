import Piece
import Joueur
import copy
import random

TAILLE=8
COULEURJOUEURBAS='blanc'
COULEURJOUEURHAUT='noir'

conversion={'A':0, 'B':1, 'C':2, 'D':3, 'E':4, 'F':5, 'G':6, 'H':7, 8:0, 7:1, 6:2, 5:3, 4:4, 3:5, 2:6, 1:7,'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6, 'h':7, }
conversiontype={'r':'roi','d':'dame','f':'fou','t':'tour','c':'cavalier','p':'pion','R':'roi','D':'dame','F':'fou','T':'tour','C':'cavalier','P':'pion'}
reconversType={'cavalier':'c', 'dame':'d','roi':'r','fou':'f','tour':'t','pion':'p'}
reconversLigne={0:8,1:7,2:6,3:5,4:4,5:3,6:2,7:1}
reconversColonne={0:'a',1:'b',2:'c',3:'d',4:'e',5:'f',6:'g',7:'h'}
adverse={0:1,1:0}

class Board:

	def __init__(self):
		self.__listejoueur=[]
		self.__joueur1=Joueur.Joueur(COULEURJOUEURBAS,'bas')
		self.__listejoueur.append(self.__joueur1)
		self.__joueur2=Joueur.Joueur(COULEURJOUEURHAUT,'haut')
		self.__listejoueur.append(self.__joueur2)


	def __str__(self):
		chaine="  A B C D E F G H \n"
		chaine=chaine+"  _______________\n"
		for ligne in range(1,TAILLE+1):
			chaine=chaine+str(TAILLE+1-ligne)+"|"
			for colonne in range(1,TAILLE+1):
				piecedanscase=False
				for joueur in self.__listejoueur:
					for piece in joueur.getlistepiece():
						if piece.getpositiony()==ligne-1 and piece.getpositionx()==colonne-1:
							chainepiece=piece.getrepresentation()
							piecedanscase=True
				if piecedanscase is True:
					if colonne==TAILLE:
						chaine=chaine+chainepiece
					else:	
						chaine=chaine+chainepiece+" "
				else:
					if colonne==TAILLE:
						chaine=chaine+" "
					else:
						chaine=chaine+"  "
			chaine=chaine+'|\n'
		chaine=chaine+' -----------------'
		return chaine


	#=================================== setter et getter ===================================#
	
	def getlistejoueur(self):
		return self.__listejoueur



	#=================================== methodes statiques pour les saisies ===================================#

	#convertie la saisie en tableau et la transforme pour que le programme comprend
	@staticmethod
	def conversionsaisie(chainesaisie):
		saisie_tableau=list(chainesaisie)
		#taille 2
		if len(saisie_tableau)==2:
			saisie_tableau[0]=conversion[saisie_tableau[0]]
			saisie_tableau[1]=conversion[int(saisie_tableau[1])]
		#taille 3
		else:
			saisie_tableau[0]=conversiontype[saisie_tableau[0]]
			saisie_tableau[1]=conversion[saisie_tableau[1]]
			saisie_tableau[2]=conversion[int(saisie_tableau[2])]
		return saisie_tableau

	#regarde si la saisie comprends des elements que le programme peut convertir
	@staticmethod
	def verificationsaisie(chainesaisie):
		saisie_tableau=list(chainesaisie)
		colonne=['A','B','C','D','E','F','G','H','a','b','c','d','e','f','g','h']
		ligne=['1','2','3','4','5','6','7','8']
		typ=['r','d','f','t','c','p','R','D','F','T','C','P']
		if len(saisie_tableau)==2:
			if saisie_tableau[0] not in colonne:
				return False
			if saisie_tableau[1] not in ligne:
				return False
		else:
			if saisie_tableau[0] not in typ: 
				return False
			if saisie_tableau[1] not in colonne:
				return False
			if saisie_tableau[2] not in ligne:
				return False
		return True

	@staticmethod
	def choixpromotion(mode):
		print("Promotion du pion, comment voulez-vous promouvoir votre pion :")
		print("1) Dame\n2) Tour\n3) Cavalier\n4) Fou")
		
		if mode==1 and quijoue==1 or mode==3:
			promotype=random.randint(1,4)
		else:
			validepromotype = False
			while validepromotype is False :
				while True:
					try:
						promotype=int(input("Votre choix : "))
						break
					except ValueError:
						print("ce n'est pas un entier")
				if promotype==1 or promotype==2 or promotype==3 or promotype==4:
					validepromotype=True
				else:
					print("erreur de saisie, pas dans la liste")
		return promotype

	#=================================== methode de recupereration d'un choix et de validation ===================================#

	#recupere la saisie 
	def saisie(self,quijoue,mode):
		saisievalide=False
		roque=False
		while saisievalide is False:

			if mode==1 and quijoue==1:
				coordonnees_chaine=self.choixIA(quijoue)
			elif mode==3:
				if quijoue==0:
					coordonnees_chaine=self.choixIACoupBerger(quijoue)
				else:
					coordonnees_chaine=self.choixIA(quijoue)
			else:
			#recuperation de la saisie

				if self.estEnEchec(quijoue) is True:
					coordonnees_chaine=input("(Sous Echec), votre coup : ")
				else:
					coordonnees_chaine=input("votre coup : ")

			#selon la saisie :
			#saisie intelligente
			if len(coordonnees_chaine)==2:
				if Board.verificationsaisie(coordonnees_chaine) is True:
					#verifier si c'est une bonne saisie
					coordonnees_tableau=Board.conversionsaisie(coordonnees_chaine)
					self.saisieintelligente(quijoue,coordonnees_tableau[0],coordonnees_tableau[1])
			
			#demande de deplacement d'une piece
			elif len(coordonnees_chaine)==3:
				if Board.verificationsaisie(coordonnees_chaine) is True:
					#verififier si c'est une bonne saisie
					coordonnees_tableau=Board.conversionsaisie(coordonnees_chaine)
					tableau_possibilite=self.saisiesimple(quijoue,coordonnees_tableau[0],coordonnees_tableau[1],coordonnees_tableau[2])
					
					#si une seule piece (du type donnée) peut attendre la case, on peut valider le mouvement
					if len(tableau_possibilite)==1:
						saisievalide=True
					
					#si aucune piece (du type donnée) ne peut atteindre la case	
					elif len(tableau_possibilite)==0:
						if (mode==1 and quijoue==0) or mode==2:
							print(f"aucun {coordonnees_tableau[0]} ne peut atteindre la position {coordonnees_chaine[1]}{coordonnees_chaine[2]}")
					
					#si plusieurs pieces (du type donnée) peuvent atteindre la case, demander au joueur quelle piece se deplace
					else:
						print("choisir quelle piece à déplacer parmi :")
						tableau_choixpiece=[]
						#recuperer les pieces
						for index in tableau_possibilite:
							current_piece=self.__listejoueur[quijoue].getlistepiece()[index]
							element_choixpiece=str(reconversColonne[current_piece.getpositionx()])+str(reconversLigne[current_piece.getpositiony()])
							tableau_choixpiece.append(element_choixpiece)
						
						#afficher les piece a déplacer
						for ch in tableau_choixpiece:
							print(ch)
						if mode==1 and quijoue==1 or mode==3:
							aleat=random.randint(0,len(tableau_choixpiece)-1)
							choixpiece=tableau_choixpiece[aleat]
						else:
							#saisie de la piece
							choixpiece=input()
							while choixpiece not in tableau_choixpiece:
								choixpiece=input("Pas dans la liste des choix, refaire la saisie : ")
						
						choixpiece_tableau=Board.conversionsaisie(choixpiece)
						
						#on recupere la piece 
						for index in tableau_possibilite:
							if choixpiece_tableau[0]==self.__listejoueur[quijoue].getlistepiece()[index].getpositionx() and choixpiece_tableau[1]==self.__listejoueur[quijoue].getlistepiece()[index].getpositiony():
								indexfinal=index
						tableau_possibilite=[indexfinal]
						saisievalide=True
			
			#commande help!
			elif coordonnees_chaine=="help!":
				self.help(quijoue)

			#commande roque
			elif coordonnees_chaine=="roque":
				#roque possible seulement si le joueur n'est pas en echec
				if self.estEnEchec(quijoue) is False:
					print("les tours qui peuvent effectuer actuellement le roque :")
					
					#verifier si un roque est possible
					roquevalide=self.validationroque(quijoue)
					if roquevalide['validite'] is True:
						
						tableau_choixtour=[]
						for index in roquevalide['liste']:
							element_choixtour=str(reconversColonne[self.__listejoueur[quijoue].getlistepiece()[index].getpositionx()])+str(reconversLigne[self.__listejoueur[quijoue].getlistepiece()[index].getpositiony()])
							tableau_choixtour.append(element_choixtour)
						for ch in tableau_choixtour:
							print(ch)
						tourroque=input("saisir quelle tour effectue le roque :")
						while tourroque not in tableau_choixtour:
							tourroque=input("Pas dans la liste des choix, refaire la saisie : ")
						tourroque_tableau=Board.conversionsaisie(tourroque)

						#on recupere la piece 
						for index in roquevalide['liste']:
							if tourroque_tableau[0]==self.__listejoueur[quijoue].getlistepiece()[index].getpositionx() and tourroque_tableau[1]==self.__listejoueur[quijoue].getlistepiece()[index].getpositiony():
								indexfinal=index
						tableau_possibilite=[indexfinal]
						coordonnees_tableau=[None,None,None]
						roque=True
						saisievalide=True
					
			#reste : erreur de saisie
			else:
				print("saisie invalide")

		if mode==1 and quijoue==1 or mode==3:
			if self.estEnEchec(quijoue) is True:
				print("ordinateur en echec")
			print(f"l'odinateur a joué : {coordonnees_chaine}")

		return {'index':tableau_possibilite[0], 'positionx':coordonnees_tableau[1], 'positiony':coordonnees_tableau[2],'roque':roque} #tableau_possibilite est de la forme [index de la piece a deplacé, colonne de la cible, ligne de la cible]



	def choixIA(self,quijoue):
		colonne=['a','b','c','d','e','f','g','h']
		ligne=['1','2','3','4','5','6','7','8']
		typ=[]
		for piece in self.__listejoueur[quijoue].getlistepiece():
			if piece.getrepresentation() not in typ:
				typ.append(piece.getrepresentation())
		choixtype=typ[random.randint(0,len(typ)-1)]
		choixligne=ligne[random.randint(0,len(ligne)-1)]
		choixcolonne=colonne[random.randint(0,len(colonne)-1)]
		choixfinal=choixtype+choixcolonne+choixligne
		return choixfinal

	#l'IA qui a developpé le coup du berger ne peut être que le joueur qui joue en bas
	def choixIACoupBerger(self,quijoue):

		listcoup=['pd4',['dc3','da5'],'ff4','dc7']

		#regarder si le coup du berger n'a pas encore été essayé
		if self.__listejoueur[quijoue].getcdb() is True:
			#savoir quel mouvement faire : regarder si la case initiale du pion, fou ou dame est vide
			#deplacement du pion : 
			etape1=True
			for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
				#si une piece se trouve en d2, si ce n'est pas le cas on peut passer à l'étape suivante
				if self.__listejoueur[quijoue].getlistepiece()[index].getpositionx()==3 and self.__listejoueur[quijoue].getlistepiece()[index].getpositiony()==6:
					etape1=False
			if etape1 is True:
				#deplacement de la reine en A5 ou C3. savoir si la dame se trouve à la case E1 
				etape2=True
				for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
					#si une piece se trouve en E1, si ce n'est pas le cas on peut passer à l'étape suivante
					if self.__listejoueur[quijoue].getlistepiece()[index].getpositionx()==4 and self.__listejoueur[quijoue].getlistepiece()[index].getpositiony()==7:
						etape2=False
				if etape2 is True:
					#deplacement du fou en F4 savoir si le fou se trouve encore en C1
					etape3=True
					for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
						#si une piece se trouve en C1, sinon on passe à l'étape suivante
						if self.__listejoueur[quijoue].getlistepiece()[index].getpositionx()==2 and self.__listejoueur[quijoue].getlistepiece()[index].getpositiony()==7:
							etape3=False
					if etape3 is True:
						
						#deplacement finale de la dame C7 : verifier si la dame atteindre la case C7
						
						#verifier si la dame est encore en jeu
						#on cherche l'index de la rene
						indexdame=-1
						for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
							if self.__listejoueur[quijoue].getlistepiece()[index].gettype()=='dame':
								indexdame=index
						#si la dame est encore en vie
						if indexdame!=-1:
							#si la rene peut atteindre C7
							if self.validationdeplacement(quijoue,indexdame,2,1) is True and self.mouvementnonechec(quijoue,indexdame,2,1) is True:
									#mouvement : dc7
									self.__listejoueur[quijoue].setcdb(False)
									return listcoup[3]
							#si la rene ne peut pas atteindre c7 depuis sa case actuelle
							else:
								#s'il ne peut pas atteindre la case c7 depuis A5 : si la dame se trouve en A5
								if self.__listejoueur[quijoue].getlistepiece()[indexdame].getpositionx()==0 and self.__listejoueur[quijoue].getlistepiece()[indexdame].getpositiony()==3:
									#regarder si la dame peut atteindre c7 depuis c3
									
									#on parcourt toute la liste des pieces adverses
									c7fromc3=True
									for piece in self.__listejoueur[quijoue].getlistepiece():
										if piece.getpositionx()==2 and piece.getpositiony()==2:
											c7fromc3=False
										if piece.getpositionx()==2 and piece.getpositiony()==3:
											c7fromc3=False
										if piece.getpositionx()==2 and piece.getpositiony()==4:
											c7fromc3=False

									#si le déplacement est possible depuis c3
									if c7fromc3 is True:
										self.__listejoueur[quijoue].setcdb(False) #le coup du berger se termine
										return listcoup[3]
									
									#sinon passer l'ia en mode aléatoire	
									else:
										self.__listejoueur[quijoue].setcdb(False)
										return self.choixIA(quijoue)
								#s'il ne peut pas atteindre la case C7 depuis C3, si la dame se trouve en c3 
								else:
									#regarser si la case B6 est occupée par une piece ennemi
									caseG6occupe=False
									for piece in self.__listejoueur[adverse[quijoue]].getlistepiece():
										if piece.getpositionx()==1 and piece.getpositiony()==2:
											caseG6occupe=True
									#si la case B6 n'est pas occupé par une piece
									if caseG6occupe is False:
										return listcoup[1][1]
									else:
										self.__listejoueur[quijoue].setcdb(False)
										return self.choixIA(quijoue)	
						#la dame n'est plus en vie
						else:
							#le coup du berger n'est plus possible : IA mode aléatoire
							self.__listejoueur[quijoue].setcdb(False)
							return self.choixIA(quijoue)
					#le fou se trouve encore en C1	
					else:
						#on fait le déplacement ff4
						return listcoup[2]
				
				#deplacement de la reine	 
				else:
					#verifier le meilleur choix entre dc3 ou da5 : toujours passer par A5. Mais s'il y une piece en B6, passer par C3
					parA5=True
					#on regarde s'il y a une piece ennemi sur B6
					for index in range(len(self.__listejoueur[adverse[quijoue]].getlistepiece())):
						if self.__listejoueur[adverse[quijoue]].getlistepiece()[index].getpositionx()==1 and self.__listejoueur[adverse[quijoue]].getlistepiece()[index].getpositiony()==2:
							parA5=False
					
					#passer par A5
					if parA5 is True:
						return listcoup[1][1]
						#mouvement à faire : da5
					
					#passer par C3
					else:
						return listcoup[1][0]
						#mouvement à faire : dc6
			#le pion se trouve toujours en D2 : faire le déplacement du pion
			else:
				#mouvement à faire : pd4
				return listcoup[0]
		
		#le coup du berger a déja été essayé -> IA mode aléatoire
		else:
			return self.choixIA(quijoue)


	#=================================== methodes de verification des deplacements ===================================#

	#savoir si une piece gene le deplacement
	def deplacementnonbloque(self,quijoue,indexpiece,positionx,positiony):
		for joueur in self.__listejoueur:
			for piece in joueur.getlistepiece():
				if self.__listejoueur[quijoue].getlistepiece()[indexpiece].deplacementnonbloquepiece(piece,positionx,positiony) is False:
					return False
		return True

	#verifie si le deplacement est possible
	def validationdeplacement(self,quijoue,indexpiece,positionx,positiony):
		joueurquijoue=self.__listejoueur[quijoue] #joueur qui deplace sa piece
		#si la cible est à la portée de la piece à déplacer
		if joueurquijoue.getlistepiece()[indexpiece].deplacementportee(positionx,positiony) is True :
			#si aucune piece empeche le deplacement
			if self.deplacementnonbloque(quijoue,indexpiece,positionx,positiony) is True:
				#si une piece allié est deja présente. 
				#On peut deplacer la piece meme s'il n'y a pas de piece ou meme s'il y a une piece adverse
				#s'il y a deja une piece allié sur la cible, deplacement impossible
				if self.__listejoueur[quijoue].pieceAlliepresente(indexpiece,positionx,positiony) is True:
					return False
				#s'il n'y a pas de piece allié sur la cible, deplacement possible
				else:
					#cas particulier du pion (ne peut qu'avancer tout droit et attaquer seulement en diagonale)
					if self.__listejoueur[quijoue].getlistepiece()[indexpiece].gettype()=='pion':

						#analyse le deplacement et verifie si le deplacement est possible (attaque diagonale, double avance, avance simple, prise en passant)
						if self.__listejoueur[quijoue].getlistepiece()[indexpiece].deplacementspecialPion(self.__listejoueur[adverse[quijoue]].getlistepiece(),positionx,positiony) is True:
							return True
					#piece de type non "pion"
					else:
						return True

			#si une piece empeche le deplacement
			else:
				return False

		#si la cible n'est pas à la portée de la piece
		else:
			return False

	#savoir si le joueur est en echec
	def estEnEchec(self,quijoue):
		#on trouve les coordonées du roi du joueur qui joue
		coordRoi=[]
		for piece in self.__listejoueur[quijoue].getlistepiece():
			if piece.gettype()=='roi':
				coordRoi.append(piece.getpositionx())
				coordRoi.append(piece.getpositiony())

		#on regarde si une piece ennemi peut atteindre cette case
		for index in range(len(self.__listejoueur[adverse[quijoue]].getlistepiece())):
			#si la piece d'index "index" peut atteindre les coordonées du roi, alors le roi est en Echec
			if self.validationdeplacement(adverse[quijoue],index,coordRoi[0],coordRoi[1]) is True:
				return True
		return False

	#savoir si apres un deplacement valide, le joueur se retrouve en echec
	def mouvementnonechec(self,quijoue,index,positionx,positiony):

		indexAsupprimer={'valide':False, 'index': None}
		#on simule le deplacement ou l'attaque et le test sur un plateau de substitut
		for index_current in range (len(self.__listejoueur[adverse[quijoue]].getlistepiece())):
			#si c'est une attaque
			if self.__listejoueur[adverse[quijoue]].getlistepiece()[index_current].getpositionx()==positionx and self.__listejoueur[adverse[quijoue]].getlistepiece()[index_current].getpositiony()==positiony:
				#on recupere l'index de la piece adverse a supprimer
				indexAsupprimer['index']=index_current	
				indexAsupprimer['valide']=True
		#utilisation de la librairie copy pour générer une double du plateau
		boardSubs=copy.deepcopy(self)		
		#on effectue les changements dans le board de test
		#on supprime la piece de l'adversaire
		if indexAsupprimer['valide'] is True:
			boardSubs.getlistejoueur()[adverse[quijoue]].getlistepiece().pop(indexAsupprimer['index'])

		#on change la position de la piece à deplacer
		boardSubs.getlistejoueur()[quijoue].changecoordonees(index,positionx,positiony)
		#on regarde si le substitut est en echec
		if boardSubs.estEnEchec(quijoue):
			nonechec = False
		else:
			nonechec = True
		return nonechec



	#=================================== methodes des differentes commandes saisisables par le joueur ===================================#

	#commande à trois caracteres : permet au programme quelle piece à deplacer selon le type de la piece et les coordonées de la cible
	def saisiesimple(self,quijoue,typepiece,positionx,positiony):
		listepiecejoueur=self.__listejoueur[quijoue].getlistepiece()
		listePieceDeplacement=[]
		for index in range (len(listepiecejoueur)):
			#si la piece est du meme type que celui de la saisie 
			if listepiecejoueur[index].gettype()==typepiece:
				#si le deplacement est possible
				if self.validationdeplacement(quijoue,index,positionx,positiony):
					#si ce deplacement entraine la mise en echec du joueur
						if self.mouvementnonechec(quijoue,index,positionx,positiony) is True:
							listePieceDeplacement.append(index)
		return listePieceDeplacement #tableau d'index des pieces (du type donnée) qui peuvent atteindre la cible

	#commande à deux caracteres : permet de savoir les déplacements possibles de la piece dans la case de positions x et y
	def saisieintelligente(self,quijoue,positionx,positiony):
		#savoir si une piece est présente dans la cible
		piecevalide=self.__listejoueur[quijoue].validationselectionpiece(positionx,positiony)

		if piecevalide['validite'] is True:
			chaine="la liste des coups pour le "+str(self.__listejoueur[quijoue].getlistepiece()[piecevalide['index']].gettype())+" en "+str(reconversColonne[positionx])+str(reconversLigne[positiony])+" est : "
			#on parcourt toutes les cases du plateau
			auMoinsUnCoup=False
			for i in range(TAILLE):
				for j in range(TAILLE):
					#si le mouvement est possible
					if self.validationdeplacement(quijoue,piecevalide['index'],i,j) is True:
						if self.mouvementnonechec(quijoue,piecevalide['index'],i,j) is True:
							auMoinsUnCoup=True
							chaine=chaine+str(reconversType[self.__listejoueur[quijoue].getlistepiece()[piecevalide['index']].gettype()])+str(reconversColonne[i])+str(reconversLigne[j])+" "
			if auMoinsUnCoup is False:
				chaine=chaine+"aucun coup possible pour cette pièce"
		else:
			chaine="pas de piece en "+str(reconversColonne[positionx])+str(reconversLigne[positiony])
		print(chaine)

	#commande help! qui permet d'afficher tous les déplacements possibles du joueur
	def help(self,quijoue):
		chaine="Les coups possibles :\n"
		#pour chaque piece du joueur
		for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
			#pour chaque case du plateau
			for i in range(TAILLE):
				for j in range (TAILLE):
					#si le placement est possible
					if self.validationdeplacement(quijoue,index,i,j) is True:
						#si le deplacement n'entraine pas la mise en echec du joueur
						if self.mouvementnonechec(quijoue,index,i,j) is True:	
							#on recupere la commande
							chaine=chaine+str(reconversType[self.__listejoueur[quijoue].getlistepiece()[index].gettype()])+str(reconversColonne[i])+str(reconversLigne[j])+" "
		print(chaine)

	#commande roque
	def validationroque(self,quijoue):
		#on cherche l'index du roi
		for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
			if self.__listejoueur[quijoue].getlistepiece()[index].gettype()=='roi':
				indexroi=index
		
		listetour=[] #liste d'index des tours qui peuvent effectuer le roque
		
		#si le roi ne s'est jamais déplacer
		if self.__listejoueur[quijoue].getlistepiece()[indexroi].getdepart()==True :
			
			#on cherche l'index des tours
			for index in range(len(self.__listejoueur[quijoue].getlistepiece())):
				if self.__listejoueur[quijoue].getlistepiece()[index].gettype()=='tour':

					#si la tour ne s'est jamais déplacer
					if self.__listejoueur[quijoue].getlistepiece()[index].getdepart()==True:

						#distinguer s'il s'agit d'un roque à gauche ou à droite
						#si c'est un roque à gauche
						if 	self.__listejoueur[quijoue].getlistepiece()[index].getpositionx()<self.__listejoueur[quijoue].getlistepiece()[indexroi].getpositionx():
							direction=-1
						#si c'est un roque à droite
						else:
							direction=1

						#regarder s'il il y a des pieces entre la tour et le roi : on regarde si la tour peut atteindre la case à coté du roi sans effectuer une attaque

						#si le deplacement de la tour vers le roi est possible
						if self.deplacementnonbloque(quijoue,index,self.__listejoueur[quijoue].getlistepiece()[indexroi].getpositionx()+direction,self.__listejoueur[quijoue].getlistepiece()[index].getpositiony()) is True:
							
							#on verifie si la case à côté du roi vers la tour concernée par le roque est occupée par une case ennemie
							deplacementnonbloquecomplet=True
							for piece in self.__listejoueur[adverse[quijoue]].getlistepiece():
								if piece.getpositionx==self.__listejoueur[quijoue].getlistepiece()[indexroi].getpositionx()+direction and piece.getpositiony ==	self.__listejoueur[quijoue].getlistepiece()[index].getpositiony():
									deplacementnonbloquecomplet = False

							#s'il n'y a pas de pieces entre la tour et le roi
							if deplacementnonbloquecomplet is True:

								#verifier si les deux cases sont ciblable par une piece ennemie
								caseciblable=False
								#on regarde s'il y a une piece ennemi qui peut atteindre les cases que le roi va franchir pendant le roque
								for i in range(1,3): #déplacement de deux cases du roi
									#pour chaque piece de l'adversaire
									for indexadverse in range(len(self.__listejoueur[adverse[quijoue]].getlistepiece())):
										#si une piece adverse peut atteindre la case 
										if self.validationdeplacement(adverse[quijoue],indexadverse,self.__listejoueur[quijoue].getlistepiece()[indexroi].getpositionx()+i*direction,self.__listejoueur[quijoue].getlistepiece()[indexroi].getpositiony()) is True:
											caseciblable=True

								#si les deux cases ne sont pas ciblables par une piece ennemi
								if caseciblable is False:
									listetour.append(index)
		if listetour==[]:
			print("Aucun roque n'est possible")
			return {'validite':False}
		else:
			return {'validite':True, 'liste':listetour}

	#=================================== Modification du plateau qui necessite les deux joueurs ===================================#


	def verificationattaque(self,quijoue,resultat):
		indexAsupprimer={'valide':False, 'index': None}
		for index in range (len(self.__listejoueur[adverse[quijoue]].getlistepiece())):
			#si c'est une attaque
			if self.__listejoueur[adverse[quijoue]].getlistepiece()[index].getpositionx()==resultat['positionx'] and self.__listejoueur[adverse[quijoue]].getlistepiece()[index].getpositiony()==resultat['positiony']:
				#on recupere l'index de la piece adverse a supprimer
				indexAsupprimer['index']=index
				indexAsupprimer['valide']=True
				return indexAsupprimer
		
		#Savoir si le pion a effectué la Prise En Passant et recuperer l'index de la piece adverse si c'est le cas:
		if 	self.__listejoueur[quijoue].getlistepiece()[resultat['index']].gettype()=="pion":
			if self.__listejoueur[quijoue].getlistepiece()[resultat['index']].getpep() is True:
				
				self.__listejoueur[quijoue].getlistepiece()[resultat['index']].setpep(False)
				#chercher la piece a supprimer
				for index in range(len(self.__listejoueur[adverse[quijoue]].getlistepiece())):
					if self.__listejoueur[adverse[quijoue]].getlistepiece()[index].getpositionx()==resultat['positionx'] and self.__listejoueur[quijoue].getlistepiece()[resultat['index']].getpositiony()==self.__listejoueur[adverse[quijoue]].getlistepiece()[index].getpositiony():
						indexAsupprimer['index']=index
						indexAsupprimer['valide']=True
						return indexAsupprimer
		return indexAsupprimer



	def annulationPEP(self,adverse):
		for piece in self.__listejoueur[adverse].getlistepiece():
			if piece.gettype()=='pion':
				if piece.getdepart()==2:
					piece.setdepart(0)



	#=================================== condition de victoire du joueur ===================================#
	
	#si le roi du joueur adverse est en echec et aucun deplacement n'est possible
	def conditionvictoire(self,adverse):
		quijoue={0:1,1:0}
		if len(self.__listejoueur[adverse].getlistepiece())==1 and len(self.__listejoueur[quijoue[adverse]].getlistepiece())==1:
			print("egalité, il reste les deux rois")
			return [True,True]

		#on cherche l'index du roi adverse
		for index_current in range(len(self.__listejoueur[adverse].getlistepiece())):
			if self.__listejoueur[adverse].getlistepiece()[index_current].gettype()=='roi':
				index=index_current

		#on regarde si le roi peut se deplacer		
		roideplacement=False
		for i in range (TAILLE):
			for j in range (TAILLE):
				#si le roi peut se deplacer à la cible (i,j)
				if self.validationdeplacement(adverse,index,i,j) is True:
					if self.mouvementnonechec(adverse,index,i,j) is True:
						roideplacement=True
		nombrecouppossible=0
		for i in range(TAILLE):
			for j in range(TAILLE):
				for index_current in range(len(self.__listejoueur[adverse].getlistepiece())):
					if self.validationdeplacement(adverse,index_current,i,j) is True:
						if self.mouvementnonechec(adverse,index_current,i,j) is True:
							nombrecouppossible=nombrecouppossible+1
		if nombrecouppossible==0 and self.estEnEchec(adverse) is False:
			print("egalité, le joueur ne peut plus faire un déplacement mais n'est pas en echec")
			return [True,True]
		else:
			if self.estEnEchec(adverse) is True and roideplacement is False :
				print(f" le joueur {self.__listejoueur[quijoue[adverse]].getcouleurjoueur()} a gagné !")
				return [True,False]
			else:
				return [False,None]
