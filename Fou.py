import Piece

class Fou(Piece.Piece):
	def __init__(self,couleur,numero,position):
		super().__init__(couleur,numero,position)
		if position=='bas':
			self._positiony=7
			self._representation='F'
		elif position=='haut':
			self._positiony=0
			self._representation='f'
		if self._numero==1:
			self._positionx=2
		elif self._numero==2:
			self._positionx=5
		self._type='fou'


	#=================================== fonction de verification selon le type de la piece ===================================#

	def deplacementportee(self,positionx,positiony):
		if abs(self._positionx-positionx)==abs(self._positiony-positiony):
			return True
		else:
			return False

	def deplacementnonbloquepiece(self,piece,positionx,positiony):
		#cas où il se compare à lui même
		if self._positionx==piece.getpositionx() and self._positiony==piece.getpositiony():
			return True
		#cas où la cible est en haut à droite de piece à deplacer
		if self._positionx<positionx and self._positiony>positiony:
			#si la "piece" se trouve dans la zone en haut à droite de la piece à déplacer 
			if piece.getpositionx()>self._positionx and piece.getpositiony()<self._positiony:
				#on verifie chaque case de la diagonale en haut à droite de la piece à deplacer jusqu'a atteindre la cible
				positionx_current=self._positionx+1
				positiony_current=self._positiony-1
				while positionx!= positionx_current and positiony!=positiony_current:
					#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
					if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
						return False
					positionx_current=positionx_current+1
					positiony_current=positiony_current-1
		#cas où la cible est en haut à gauche de la piece à déplacer
		elif self._positionx>positionx and self._positiony>positiony:
			#si la "piece" se trouve dans la zone en haut à gauche de la piece à déplacer 
			if piece.getpositionx()<self._positionx and piece.getpositiony()<self._positiony:
				#on verifie chaque case de la diagonale en haut à gauche de la piece à deplacer jusqu'a atteindre la cible
				positionx_current=self._positionx-1
				positiony_current=self._positiony-1
				while positionx!= positionx_current and positiony!=positiony_current:
					#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
					if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
						return False
					positionx_current=positionx_current-1
					positiony_current=positiony_current-1
		#cas où la cible est en bas à gauche de la piece à déplacer
		elif self._positionx>positionx and self._positiony<positiony:
			#si la "piece" se trouve dans la zone en bas à gauche de la piece à déplacer 
			if piece.getpositionx()<self._positionx and piece.getpositiony()>self._positiony:
				#on verifie chaque case de la diagonale en bas à gauche de la piece à deplacer jusqu'a atteindre la cible
				positionx_current=self._positionx-1
				positiony_current=self._positiony+1
				while positionx!= positionx_current and positiony!=positiony_current:
					#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
					if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
						return False
					positionx_current=positionx_current-1
					positiony_current=positiony_current+1
		#cas où la cible est en bas à droite de la cible
		elif self._positionx<positionx and self._positiony<positiony:
			#si la "piece" se trouve dans la zone en bas à droite de la piece à déplacer 
			if piece.getpositionx()>self._positionx and piece.getpositiony()>self._positiony:
				#on verifie chaque case de la diagonale en bas à droite de la piece à deplacer jusqu'a atteindre la cible
				positionx_current=self._positionx+1
				positiony_current=self._positiony+1
				while positionx!= positionx_current and positiony!=positiony_current:
					#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
					if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
						return False
					positionx_current=positionx_current+1
					positiony_current=positiony_current+1
		else:	
			return False
		return True
