import Piece
class Tour(Piece.Piece):
	def __init__(self,couleur,numero,position):
		super().__init__(couleur,numero,position)
		if position=='bas':
			self._positiony=7
			self._representation='T'
		elif position=='haut':
			self._positiony=0
			self._representation='t'
		if numero==1:
			self._positionx=0
		else:
			self._positionx=7
		self._type='tour'
		self.__depart=True	#permet de savoir s'il puisse faire le roque
		self.__roque=False	#permet de savoir lorsque un roque est effectué, lequel des tours l'executent


	#setter et getter

	def getdepart(self):
		return self.__depart

	def setdepart(self,depart):
		self.__depart=depart

	def deplacementportee(self,positionx,positiony):
		if self._positionx==positionx or self._positiony==positiony:
			return True
		else:
			return False

	#=================================== fonctions de verification selon le type de la piece ===================================#

	## self : piece à deplacer, piece :"piece", positionx positiony : cible
	def deplacementnonbloquepiece(self,piece,positionx,positiony):
		#cas où il se compare à lui même
		if self._positionx==piece.getpositionx() and self._positiony==piece.getpositiony():
			return True
		#cas où la piece à deplacer et la cible sont dans la meme colonne 
		if positionx==self._positionx:
			#si la "piece" est dans la meme colonne que la cible et la piece à deplacer
			if piece.getpositionx()==self._positionx:
				#permet d'organiser le "for"
				if positiony>self._positiony:
					#on regarde pour chaque case entre la cible et la piece à deplacer
					for i in range (self._positiony,positiony):
						#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
						if piece.getpositiony()==i:
							return False
				#permet d'organiser le "for"
				else:
					#on regarde pour chaque case entre la cible et la piece à deplacer
					for i in range(positiony+1,self._positiony):
						#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
						if piece.getpositiony()==i:
							return False
		#cas où la piece à déplacer et la cible sont dans la meme ligne
		else:
			#si la "piece" est dans la meme ligne que la cible et la piece à déplacer
			if piece.getpositiony()==self._positiony:
				#permet d'organiser le "for"
				if positionx>self._positionx:
					#on regarde pour chaque case entre la cible et la piece à deplacer
					for i in range (self._positionx,positionx):
						#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
						if piece.getpositionx()==i:
							return False
				#permet d'organiser le "for"
				else:
					#on regarde pour chaque case entre la cible et la piece à deplacer
					for i in range(positionx,self._positionx):
						#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
						if piece.getpositionx()==i:
							return False
		return True

