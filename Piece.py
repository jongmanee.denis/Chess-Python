import abc

class Piece(abc.ABC):

	def __init__(self,couleur,numero,position):
		self._position=position
		self._couleur=couleur
		self._numero=numero
		self._positionx=None
		self._positiony=None
		self._representation=None
		self._type=None


	#=================================== setter et getter ===================================#
	def setpositiony(self,positiony):
		self._positiony=positiony

	def setpositionx(self,positionx):
		self._positionx=positionx

	def getpositionx(self):
		return self._positionx

	def getpositiony(self):
		return self._positiony

	def getrepresentation(self):
		return self._representation

	def getcouleur(self):
		return self._couleur

	def gettype(self):
		return self._type

	#=================================== abstractmethod ===================================#

	@abc.abstractmethod
	def deplacementportee(self):
		pass
	@abc.abstractmethod
	def deplacementnonbloquepiece(self):
		pass

