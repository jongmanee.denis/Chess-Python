import Piece

class Cheval(Piece.Piece):
	def __init__(self,couleur,numero,position):
		super().__init__(couleur,numero,position)
		if position=='bas':
			self._representation='C'
			self._positiony=7
		if position=='haut':
			self._positiony=0
			self._representation='c'
		if numero==1:
			self._positionx=1
		else:
			self._positionx=6
		self._type='cavalier'

	#=================================== fonctions de verification selon le type de la piece ===================================#

	def deplacementportee(self,positionx,positiony):
		if abs(self._positiony-positiony)==1:
			if abs(self._positionx-positionx)==2:
				return True
			else:
				return False
		elif abs(self._positiony-positiony)==2:
			if abs(self._positionx-positionx)==1:
				return True
			else:	
				return False
		else:
			return False

	def deplacementnonbloquepiece(self,piece,positionx,positiony):
		return True
