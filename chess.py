import Board
import random
joueuradverse={0:1,1:0}




def chess(mode):
	print("================================== Bienvenue dans le jeu d'echecs ! ==================================\n\n")
	
	print("liste des commandes possibles :")
	print("3 caratères : faire un coup")
	print("2 caractères : saisie intelligente (voir les coups possibles de la pièce")
	print("help! : afficher tous les coups possibles")
	print("roque : executer un roque\n\n")
	
	#configuration de la partie
	plateau=Board.Board()
	print("Choisir le mode de jeu:")
	print("1) Joueur vs Ordinateur\n2) Joueur vs Joueur\n3) Ordinateur (coup du berger puis aléatoire) vs Ordinateur (aléatoire)")
	#verification de la saisie
	modevalide = False
	while modevalide is False:
		while True:
			try:
				mode=int(input())
				break
			except ValueError:
				print("erreur de saisie")
		if mode==1 or mode==2 or mode==3:
			modevalide = True
	gagnant=[False,None]
	quijoue=0

	print("\n")
	#debut de partie
	while gagnant[0] is False:
		print(plateau)
		print('\n')
	
		print(f"Joueur {plateau.getlistejoueur()[quijoue].getcouleurjoueur()}, c'est votre tour")
		resultat=plateau.saisie(quijoue,mode) 

		
		#effectuer les modifications

		#si le joueur effectue un roque
		if resultat['roque'] is True:  #resultat du type [index tour, None, None ,True]
			plateau.getlistejoueur()[quijoue].roque(resultat)	
		#pas de roque
		else:
			#changement des self.__depart si necessaire
			plateau.getlistejoueur()[quijoue].changementdepart(resultat)

			#verifier si c'est une attaque
			indexAsupprimer=plateau.verificationattaque(quijoue,resultat)
			#modifications du plateau
			if indexAsupprimer['valide'] is True:
				#supprime la piece capturée
				plateau.getlistejoueur()[joueuradverse[quijoue]].supprimerpion(indexAsupprimer['index'])

			#on change la position de la piece à deplacer
			plateau.getlistejoueur()[quijoue].changecoordonees(resultat['index'],resultat['positionx'],resultat['positiony'])
			#fin attaque

		#promotion du pion
		#savoir si le pion a atteint la derniere ligne et peut effectuer une promotion
		if plateau.getlistejoueur()[quijoue].getlistepiece()[resultat['index']].gettype()=='pion':
			#si le pion peut avoir une promotion
			if plateau.getlistejoueur()[quijoue].getlistepiece()[resultat['index']].promotion() is True:
				promotype=Board.Board.choixpromotion(mode)
				plateau.getlistejoueur()[quijoue].validationpromotionpion(resultat['index'],promotype)
		

		#prise en passant:
		#s'il existe un pion adverse susceptible de se faire capturer par la prise en passant, il ne le pourra pas par la suite
		plateau.annulationPEP(joueuradverse[quijoue])
		print("\n")
		
		#condition de victoire
		gagnant=plateau.conditionvictoire(joueuradverse[quijoue])
		if gagnant[0] is False:
			quijoue=joueuradverse[quijoue]
		else:
			print(plateau)
			if gagnant[1] is True:
				return 2
			else:
				return quijoue
		print("\n\n\n\n")

chess(3)
