import Piece

class Roi(Piece.Piece):
	def __init__(self,couleur,numero,position):
		super().__init__(couleur,numero,position)
		if position=='bas':
			self._positiony=7
			self._representation='R'
		elif position=='haut':
			self._positiony=0
			self._representation='r'
		self._positionx=3
		self._type='roi'
		self.__depart=True #permet de savoir s'il puisse faire le roque

	#=================================== setter et getter ===================================#

	def getdepart(self):
		return self.__depart

	def setdepart(self,depart):
		self.__depart=depart

	#=================================== fonctions de verification selon le type de la piece ===================================#

	def deplacementportee(self,positionx,positiony):
		if (positionx>=self._positionx-1 and positionx<=self._positionx+1) and (positiony>=self._positiony-1 and positiony<=self._positiony+1):
			return True
		else:
			return False

	def deplacementnonbloquepiece(self,piece,positionx,positiony):
		return True