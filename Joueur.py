import Pion
import Cheval
import Tour
import Fou
import Dame
import Roi

class Joueur:
	def __init__(self,couleur,position):
		self._couleurjoueur=couleur
		self.__position=position
		self.__listepiece=[]
		for i in range(1,9):
			pion=Pion.Pion(couleur,i,position)
			self.__listepiece.append(pion)
		for i in range(1,3):
			cheval=Cheval.Cheval(couleur,i,position)
			self.__listepiece.append(cheval)
		for i in range(1,3):
			tour=Tour.Tour(couleur,i,position)
			self.__listepiece.append(tour)
		for i in range(1,3):
			fou=Fou.Fou(couleur,i,position)
			self.__listepiece.append(fou)
		dame=Dame.Dame(couleur,i,position)
		self.__listepiece.append(dame)
		roi=Roi.Roi(couleur,i,position)
		self.__listepiece.append(roi)	

		self.__cdb=True

	#=================================== setter et getter ===================================#
	def getcouleurjoueur(self):
		return self._couleurjoueur
	def getlistepiece(self):
		return self.__listepiece
	def setlistepiece(self,listepiece):
		self.__listepiece=listepiece

	def getcdb(self):
		return self.__cdb
	def setcdb(self,cdb):
		self.__cdb=cdb

	#=================================== autres fonctions qui s'appliquent à un seul joueur ===================================#

	#savoir si une piece appartement au joueur qui joue se trouve dans la case (positionx,positiony)
	def validationselectionpiece(self,positionx,positiony):
		for index in range (len(self.__listepiece)):
			if self.__listepiece[index].getpositionx()==positionx and self.__listepiece[index].getpositiony()==positiony:
				return {'validite':True, 'index':index}
		return {'validite':False}


	#savoir si une piece alliée est deja présente sur la case
	def pieceAlliepresente(self,indexpiece,positionx,positiony):

		#on parcourt toute la liste de pieces du joueur qui joue
		for piece in self.__listepiece:
			#s'il y a une piece alliée sur la cible
			if piece.getpositionx()==positionx and piece.getpositiony()==positiony:
				return True
		return False


	#=================================== Modification du plateau qui necessite un joueur ===================================#

	#on crée une nouvelle piece et on la remplace dans la liste de pieces
	def validationpromotionpion(self,indexpion,typeprom):
		#creation de la nouvelle piece
		if typeprom==1:
			print("promotion en Dame !")
			nouvellepiece=Dame.Dame(self._couleurjoueur,3,self.__position)
		elif typeprom==2:
			print("promotion en Tour !")
			nouvellepiece=Tour.Tour(self._couleurjoueur,3,self.__position)
			nouvellepiece.setdepart(0)
		elif typeprom==3:
			print("promotion en Cavalier !")
			nouvellepiece=Cheval.Cheval(self._couleurjoueur,3,self.__position)
		elif typeprom==4:
			print("promotion en Fou !")
			nouvellepiece=Fou.Fou(self._couleurjoueur,3,self.__position)

		#la piece recupere les coordonées du pion
		nouvellepiece.setpositionx(self.__listepiece[indexpion].getpositionx())
		nouvellepiece.setpositiony(self.__listepiece[indexpion].getpositiony())
		#on supprime le pion de la liste
		self.__listepiece.pop(indexpion)
		#on ajoute la nouvelle piece dans la liste
		self.__listepiece.append(nouvellepiece)


	def roque(self,resultat):
		#changer self.__depart du roi et de la tour
		for index in range(len(self.__listepiece)):
			if self.__listepiece[index].gettype()=='roi':
				indexroi=index	
		self.__listepiece[resultat['index']].setdepart(False)
		self.__listepiece[indexroi].setdepart(False)
		#si c'est un roque à gauche
		if self.__listepiece[resultat['index']].getpositionx()<self.__listepiece[indexroi].getpositionx():
			direction=-1
		else:
			direction=1
		self.__listepiece[indexroi].setpositionx(self.__listepiece[indexroi].getpositionx()+2*direction)
		self.__listepiece[resultat['index']].setpositionx(self.__listepiece[indexroi].getpositionx()-1*direction)

	def changementdepart(self,resultat):

		#changer self.__depart pion ou tour ou roi
		if self.__listepiece[resultat['index']].gettype()=='pion':
			#s'il avance de deux cases : peut se faire capturer par la prise en passant
			if abs(resultat['positiony']-self.__listepiece[resultat['index']].getpositiony())==2:
				self.__listepiece[resultat['index']].setdepart(2)
			#sinon on annule sa possibilité d'avancer de deux cases à l'avant
			else:
				self.__listepiece[resultat['index']].setdepart(0)
		elif self.__listepiece[resultat['index']].gettype()=='roi' or self.__listepiece[resultat['index']].gettype()=='tour':
				self.__listepiece[resultat['index']].setdepart(False)
			

	def supprimerpion(self,index):
		self.__listepiece.pop(index)

	def changecoordonees(self,index,positionx,positiony):
		self.__listepiece[index].setpositionx(positionx)
		self.__listepiece[index].setpositiony(positiony)