import Piece

class Pion(Piece.Piece):

	def __init__(self,couleur,numero,position):
		super().__init__(couleur,numero,position)
		if position=='bas':
			self._representation='P'
			self._positiony=6
		elif position=='haut':
			self._representation='p'
			self._positiony=1
		self._positionx=numero-1
		self.__depart=1 #1:peut avancer de deux cases, 2: peut se faire capturer par la prise en passant, 0:aucune possibilité
		self._type='pion'
		self.__pep=False # permet de savoir si le pion a effectué la prise en passant

	
	#changements de self.__depart :
	# - pion avance de deux cases : 1->2 (alliée)
	# - pion attaque diretement ou avance de une case (depuis sa case de départ) : 1->0 (alliée)
	# - pion qui ne se fait pas capturer et était susceptible de se faire capturer par la prise en passant : 2->0 (adversaire)


	#setter et getter
	def setdepart(self,depart):
		self.__depart=depart

	def setpep(self,pep):
		self.__pep=pep
	
	def getdepart(self):
		return self.__depart
	
	def getpep(self):
		return self.__pep

	#=================================== fonctions de verification selon le type de la piece ===================================#

	def deplacementportee(self,positionx,positiony,):
		if self._positionx==positionx:
			#si le pion est en bas (donc ne peut que monter)
			if self._position=='bas':
				#si le pion est à la case de départ
				if self.__depart==1:
					#si la cible se trouve devant à 2 cases max du pion
					if self._positiony-positiony<=2 and self._positiony-positiony>0:
						return True
					else:
						return False
				#si le pion n'est pas à la case de départ
				else:
					#si la cible se trouve juste devant 
					if self._positiony-positiony==1:
						return True
					else:
						return False
			#si le pion est en haut (donc ne peut que descendre)
			else:
				#si le pion est à la case de départ
				if self.__depart==1:
					#si la cible se trouve devant à 2 case max du pion
					if positiony-self._positiony<=2 and positiony-self._positiony>0:
						return True
					else:
						return False
				#si le pion n'est pas à la case de départ 
				else:
					if positiony-self._positiony==1:
						return True
					else:
						return False
		#portee d'attaque du pion
		elif self._positionx-1==positionx or self._positionx+1==positionx:
			#le pion ne peut que monter
			if self._position=='bas':
				#si la cible se trouve en diagonale en face du pion
				return self._positiony-1==positiony
			#le pion ne peut que descendre
			else:
				#si la cible se trouve en diagonale en face du pion
				return self._positiony+1==positiony
		else:
			return False


	def deplacementnonbloquepiece(self,piece,positionx,positiony):
		#cas où il se compare à lui même
		if self._positionx==piece.getpositionx() and self._positiony==piece.getpositiony():
			return True
		#si ils sont sur la meme colonne
		if piece.getpositionx()==positionx:
			if self._position=='bas':		
				for i in range(positiony+1,self._positiony):
					if piece.getpositiony()==i:
						return False
			elif self._position=='haut':
				for i in range(self._positiony+1,positiony):
					if piece.getpositiony()==i:
						return False
		return True

	def deplacementspecialPion(self,listePieceAdverse,positionx,positiony):
		
		pieceadversedanscible=False
		for piece in listePieceAdverse:
			if piece.getpositionx()==positionx and piece.getpositiony()==positiony:
				pieceadversedanscible=True
				
		#case occupé par une piece adverse
		if pieceadversedanscible is True:
			#Si sur la cible c'est une piece adverse : si la cible est dans la meme colonne : impossible
			#Si sur la cible c'est une piece adverse : si la cible n'est pas dans la meme colonne : donc diagonale : possible
			return positionx!=self._positionx
		#case vide
		else:
			#si la cible est vide : si la cible n'est pas dans la meme colonne : impossible
			#Si la cible est vide : si la cible est dans la meme colonne : possible
			if positionx==self._positionx:
				return True
			else:
				#regarder s'il y a un pion qui peut se faire capturer par la prise en passant et qu'il s'agit d'un pion
				for piece in listePieceAdverse:
					if piece.getpositionx()==positionx and piece.getpositiony()==self._positiony and piece.gettype()=='pion':
						if piece.getdepart()==2:
							self.__pep=True
							print("prise en passant !")
							return True
				return False

	def promotion(self):
		#si c'est le joueur du bas
		if self._position=='bas':
			#si le pion se trouve dans la deniere ligne
			return self._positiony==0
		else:
			return self._positiony==7
