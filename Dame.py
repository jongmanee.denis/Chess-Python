import Piece

class Dame(Piece.Piece):
	def __init__(self,couleur,numero,position):
		super().__init__(couleur,numero,position)
		if position=='bas':
			self._representation='D'
			self._positiony=7
		elif position=='haut':
			self._positiony=0
			self._representation='d'
		self._positionx=4
		self._type='dame'


	#=================================== fonctions de verification selon le type de la piece ===================================#

	def deplacementportee(self,positionx,positiony):
		if (self._positionx==positionx) or (self._positiony==positiony)or abs(self._positiony-positiony)==abs(self._positionx-positionx):
			return True
		else:
			return False

	#Dame est une tour et fou en meme temps. on verifie si le deplacement est un comportement de tour sinon c'est un comportement de fou
	def deplacementnonbloquepiece(self,piece,positionx,positiony):
		#cas où il se compare à lui même
		if self._positionx==piece.getpositionx() and self._positiony==piece.getpositiony():
			return True
		#si comportement de tour

		if self._positionx==positionx or self._positiony==positiony:
			#cas où la piece à deplacer et la cible sont dans la meme colonne 
			if positionx==self._positionx:
				#si la "piece" est dans la meme colonne que la cible et la piece à deplacer
				if piece.getpositionx()==self._positionx:
					#permet d'organiser le "for"
					if positiony>self._positiony:
						#on regarde pour chaque case entre la cible et la piece à deplacer
						for i in range (self._positiony,positiony):
							#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
							if piece.getpositiony()==i:
								return False
					#permet d'organiser le "for"
					else:
						#on regarde pour chaque case entre la cible et la piece à deplacer
						for i in range(positiony+1,self._positiony):
							#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
							if piece.getpositiony()==i:
								return False
			#cas où la piece à déplacer et la cible sont dans la meme ligne
			else:
				#si la "piece" est dans la meme ligne que la cible et la piece à déplacer
				if piece.getpositiony()==self._positiony:
					#permet d'organiser le "for"
					if positionx>self._positionx:
						#on regarde pour chaque case entre la cible et la piece à deplacer
						for i in range (self._positionx,positionx):
							#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
							if piece.getpositionx()==i:
								return False
					#permet d'organiser le "for"
					else:
						#on regarde pour chaque case entre la cible et la piece à deplacer
						for i in range(positionx,self._positionx):
							#si la "piece" est entre la cible et la piece à déplacer alors deplacement impossible
							if piece.getpositionx()==i:
								return False
		#comportement fou
		else:
			#cas où la cible est en haut à droite de piece à deplacer
			if self._positionx<positionx and self._positiony>positiony:
				#si la "piece" se trouve dans la zone en haut à droite de la piece à déplacer 
				if piece.getpositionx()>self._positionx and piece.getpositiony()<self._positiony:
					#on verifie chaque case de la diagonale en haut à droite de la piece à deplacer jusqu'a atteindre la cible
					positionx_current=self._positionx+1
					positiony_current=self._positiony-1
					while positionx!= positionx_current and positiony!=positiony_current:
						#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
						if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
							return False
						positionx_current=positionx_current+1
						positiony_current=positiony_current-1
			#cas où la cible est en haut à gauche de la piece à déplacer
			elif self._positionx>positionx and self._positiony>positiony:
				#si la "piece" se trouve dans la zone en haut à gauche de la piece à déplacer 
				if piece.getpositionx()<self._positionx and piece.getpositiony()<self._positiony:
					#on verifie chaque case de la diagonale en haut à gauche de la piece à deplacer jusqu'a atteindre la cible
					positionx_current=self._positionx-1
					positiony_current=self._positiony-1
					while positionx!= positionx_current and positiony!=positiony_current:
						#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
						if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
							return False
						positionx_current=positionx_current-1
						positiony_current=positiony_current-1
			#cas où la cible est en bas à gauche de la piece à déplacer
			elif self._positionx>positionx and self._positiony<positiony:
				#si la "piece" se trouve dans la zone en bas à gauche de la piece à déplacer 
				if piece.getpositionx()<self._positionx and piece.getpositiony()>self._positiony:
					#on verifie chaque case de la diagonale en bas à gauche de la piece à deplacer jusqu'a atteindre la cible
					positionx_current=self._positionx-1
					positiony_current=self._positiony+1
					while positionx!= positionx_current and positiony!=positiony_current:
						#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
						if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
							return False
						positionx_current=positionx_current-1
						positiony_current=positiony_current+1
			#cas où la cible est en bas à droite de la cible
			elif self._positionx<positionx and self._positiony<positiony:
				#si la "piece" se trouve dans la zone en bas à droite de la piece à déplacer 
				if piece.getpositionx()>self._positionx and piece.getpositiony()>self._positiony:
					#on verifie chaque case de la diagonale en bas à droite de la piece à deplacer jusqu'a atteindre la cible
					positionx_current=self._positionx+1
					positiony_current=self._positiony+1
					while positionx!= positionx_current and positiony!=positiony_current:
						#si la "piece" se trouve entre la cible et la piece à déplacer, deplacement impossible
						if positionx_current==piece.getpositionx() and positiony_current==piece.getpositiony():
							return False
						positionx_current=positionx_current+1
						positiony_current=positiony_current+1
			else:
				print("cas impossible")
				return False
		return True

